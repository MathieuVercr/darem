var express = require('express');
var passport = require('passport');
var FacebookTokenStrategy = require('passport-facebook-token');
var FacebookStrategy = require('passport-facebook').Strategy;
const mongoose = require('mongoose');
const relationship = require("mongoose-relationship");
const User = require('../models/user');
var router = express.Router();

//ROUTE FOR MOBILE FACEBOOK
router.post('/auth/facebook/token', passport.authenticate('facebook-token', { session: false }), function(req, res) {
  if (req.user) {
    User.find({ 'facebook.id': req.user.id }, function(err, users){
      if(err){
        res.status(500).send();
      }else{
        if(users.length == 0){
          var user = new User();
          user.familyName = req.user.profile.name.familyName;
          user.givenName = req.user.profile.name.givenName;
          user.email = "GEEN MAIL ATM!!";
          user.facebook.id = req.user.profile.id;
          user.facebook.photo = req.user.profile.photos[0].value;
          user.points = "";
          user.save(function(err, savedObject){
            if(err){
              console.log(err);
              res.status(500).send();
            }else{
              res.send(savedObject);
            }
          });
        }else{
          res.send(users[0].facebook.id);
        }
      }
    })
  } else {
    res.send(401);
  }
});

//PASSPORT FACEBOOK MOBILE
passport.use(new FacebookTokenStrategy({
  clientID: "398917810525601",
  clientSecret: "24b5f0087f466061f46d49f3d7c066e9"
}, function(accessToken, refreshToken, profile, done) {
  var user = {
    'email': profile.emails[0].value,
    'name': profile.name.givenName + ' ' + profile.name.familyName,
    'id': profile.id,
    'token': accessToken,
    'profile': profile,
    'points': 0
  }

  return done(null, user);
}));

//PASSPORT FACEBOOK BROWSER
passport.use(new FacebookStrategy({
    clientID: "398917810525601",
    clientSecret: "24b5f0087f466061f46d49f3d7c066e9",
    callbackURL: "https://boiling-dawn-34823.herokuapp.com/users/auth/facebook/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    console.log(profile);
  }
));

//ROUTE FOR BROWSER FACEBOOK
router.get('/auth/facebook',
  passport.authenticate('facebook'));

router.get('/auth/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    console.log("we zijn er!!");
  });

module.exports = router;
