const express = require('express');
const mongoose = require('mongoose');
const relationship = require("mongoose-relationship");
const router = express.Router();
const User = require('../models/user');
const Challenge = require('../models/challenge');
const FriendRequest = require('../models/friendRequest');


/* connectie mongoose */
//mongoose.connect('mongodb://daremAdmin:daremdemo123@ds255455.mlab.com:55455/darem');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST USER via postman */
router.post('/user/add', function(req, res, next){
	console.log("USER AAN HET TOEVOEGEN")

	var name = req.body.name;
	var voornaam = req.body.voornaam;
	var email = req.body.email;
	var points = req.body.points;

	if(name != null && email != null){
		var user = new User();
		user.name = name;
		user.voornaam = voornaam;
		user.email = email;
		user.points = points;
		user.save(function(err, savedObject){
			if(err){
				console.log(err);
				res.status(500).send();
			}else{
				res.send(savedObject);
			}
		});
	}else{
		res.status(401).send();
	}
});

/* POST Challenge via postman */
router.post('/challenge/add', function(req, res, next){
	console.log("CHALLENGE AAN HET TOEVOEGEN")

	var name = req.body.name;
	var description = req.body.description;
	var category = req.body.category;
	var creatorId = req.body.creatorId;
	var isCompleted = req.body.isCompleted;
	var users = req.body.users;


	if(name != null && creatorId != null){
		var challenge = new Challenge();
		challenge.name = name;
		challenge.description = description;
		challenge.category = category;
		challenge.creatorId = creatorId;
		challenge.isCompleted = isCompleted;
		challenge.users = users;

		challenge.save(function(err, savedObject){
			if(err){
				console.log(err);
				res.status(500).send();
			}else{
				res.send(savedObject);
			}
		});
	}else{
		res.status(401).send();
	}
});

/* DELETE Challenge via postman */
router.delete('/challenge/delete/:id', function(req, res, next){
	var id = req.params.id;

	Challenge.findOne({_id: id}, function(err, foundData){
		var challenge = new Challenge({});
		challenge = foundData;
		challenge.remove();
		res.send("OK");
	});
});

/* GET Challenge */
router.get('/challenge',function(req, res, next){
	Challenge.find({}, function(err, challenges){
		if(err){
			res.status(500).send();
		}else{
			var responseObject = [];

			if(challenges.length == 0){
				responseObject = undefined;
				res.status(400).send(responseObject);
			}else{
				responseObject.push(challenges);
				res.send(responseObject);
			}
		}
	})
});

/* GET users */
router.get('/users',function(req, res, next){
	User.find({}, function(err, users){
		if(err){
			res.status(500).send();
		}else{
			var responseObject = [];

			if(users.length == 0){
				responseObject = undefined;
				res.status(400).send(responseObject);
			}else{
				responseObject.push(users);
				res.send(responseObject);
			}
		}
	})
});

/* GET users met ID en hun volledig challenge object*/
router.get('/users/:id',function(req, res, next){
	var id = req.params.id;

	User.findOne({_id: id}, function(err, user){
		if(err){
			res.status(500).send();
		}else{
			var responseObject = [];

			if(user.length == 0){
				responseObject = undefined;
				res.status(400).send(responseObject);
			}else{
				if(user["challenges"].length >= -1){
					for (var i = 0; i <= user["challenges"].length - 1; i++) {
						Challenge.find({_id: user["challenges"][i]}, function(err, foundData){
							if(err){
								res.status(500).send();
							}else{
								user["challengesArray"].push(foundData[0]);
								if(user["challengesArray"].length === user["challenges"].length){
									responseObject.push(user);
									res.send(responseObject);
								}
							}
						});
					}
				}else{
					responseObject.push(user);
					res.send(responseObject);
				}
			}
		}
	})
});

/* GET challenge met ID en hun volledige user array object*/
router.get('/challenge/:id',function(req, res, next){
	var id = req.params.id;

	Challenge.findOne({_id: id}, function(err, foundChallenge){
		if(err){
			res.status(500).send();
		}else{
			var responseObject = [];

			if(foundChallenge.length == 0){
				responseObject = undefined;
				res.status(400).send(responseObject);
			}else{
				if(foundChallenge["users"].length >= -1){
					for (var i = 0; i <= foundChallenge["users"].length - 1; i++) {
						User.find({_id: foundChallenge["users"][i]}, function(err, foundData){
							if(err){
								res.status(500).send();
							}else{
								foundChallenge["usersArray"].push(foundData[0]);
								if(foundChallenge["usersArray"].length === foundChallenge["users"].length){
									responseObject.push(foundChallenge);
									res.send(responseObject);
								}
							}
						});
					}
				}else{
					responseObject.push(foundChallenge);
					res.send(responseObject);
				}
			}
		}
	})
});


/* Add friendsRequest */
router.post('/friendrequest', function(req, res, next){

	var requester = req.body.requester;
	var recipient = req.body.recipient;
	//Status -> 1 = requested, 2 = accepted, 3 = rejected
	var status = req.body.status;

	if(requester != null && recipient != null && status != null){
		var friendRequest = new FriendRequest();
		friendRequest.requester = requester;
		friendRequest.recipient = recipient;
		friendRequest.status = status;

		friendRequest.save(function(err, savedObject){
			if(err){
				console.log(err);
				res.status(500).send();
			}else{
				res.send(savedObject);
			}
		});
	}else{
		res.status(401).send();
	}
});

/* See your friendRequest */
router.get('/friendrequest/:id', function(req, res, next){
	var id = req.params.id;

	FriendRequest.find({recipient: id}, function(err, foundData){
		if(err){
			res.status(500).send();
		}else{
			res.send(foundData);
		}
	});
});

/* Accept or deny freindRequest */
router.put('/friendrequest/response', function(req, res, next){

	console.log(req.body);
	var requestID = req.body.requestID;
	var requester = req.body.requester;
	var recipient = req.body.recipient;
	//Status -> 1 = requested, 2 = accepted, 3 = rejected
	var status = req.body.status;



	if(requester != null && recipient != null && status != null){
		FriendRequest.findOne({_id: requestID}, function(err, foundData){
			if(err){
				console.log(err);
				res.status(500).send();
			}else{
				foundData.status = status;
				foundData.save(function(err, updatedObject){
					if(err){
						console.log(err)
						res.status(500).send();
					}else{
						res.send(updatedObject);
					}
				});
			}
		})
	}else{
		res.status(401).send();
	}
});



module.exports = router;
