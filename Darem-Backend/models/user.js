const mongoose = require('mongoose');
var relationship = require("mongoose-relationship");
const Schema = mongoose.Schema;
const challenge = require('./challenge');

//new User Schema & model
const userSchema = new Schema({
	familyName: String,
	givenName: String,
	email: String,
	facebook: {id: String, photo: String},
	points: String,
	challenges: [{type: Schema.ObjectId, ref: "Challenge", childPath: "users"}],
	challengesArray: [],
	friends: []
});

userSchema.plugin(relationship, {relationshipPathName:'challenges'});

var User = mongoose.model("User", userSchema);

module.exports = User;
