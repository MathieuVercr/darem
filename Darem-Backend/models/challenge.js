const mongoose = require('mongoose');
var relationship = require("mongoose-relationship");
const Schema = mongoose.Schema;
const user = require("./user");

//new Challenge Schema & model
const challengeSchema = new Schema({
	name: String,
	description: String,
	category: String,
	creatorId: String,
	IsCompleted: Boolean, 
	users: [{type: Schema.ObjectId, ref: "User", childPath: "challenges"}],
	usersArray: []
});

challengeSchema.plugin(relationship, {relationshipPathName:'users'});

var Challenge = mongoose.model("Challenge", challengeSchema);

module.exports = Challenge;