const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//new FriendRequest Schema & model
const friendRequestSchema = new Schema({
	requester: {
    	type: String,
    	required: true
	},
	recipient: {
    	type: String,
    	required: true
	},
	status: {
    	type: String,
    	required: true 
	}
});

var FriendRequest = mongoose.model("FriendRequest", friendRequestSchema);

module.exports = FriendRequest;
